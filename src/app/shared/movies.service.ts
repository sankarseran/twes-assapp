import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class MovieService {

    movieChange: EventEmitter<number> = new EventEmitter();
    searchTxt: EventEmitter<number> = new EventEmitter();

    constructor(private http: HttpClient) { }

    getTrendingMovies(page): Promise<any> {
        return this.http.get(environment.basePath + 'trending/movie/week?api_key=' + environment.apiKey + '&page=' + page).toPromise();
    }

    searchMovies(page, query): Promise<any> {
        return this.http.get(environment.basePath + 'search/movie?api_key=' + environment.apiKey + 
            '&language=en-US&include_adult=false&query=' + query + '&page=' + page).toPromise();
    }

    getMovie(id): Promise<any> {
        return this.http.get(environment.basePath + 'movie/' + id + '?api_key=' + environment.apiKey).toPromise();
    }

    // Movie change emitter and get event.
    emitMovieEvent(movie) {
        this.movieChange.emit(movie);
    }

    getMovieEmitter() {
        return this.movieChange;
    }

    // Movie search emitter and get event.
    emitSearchEvent(txt) {
        this.searchTxt.emit(txt);
    }

    getSearchEmitter() {
        return this.searchTxt;
    }
}