import { Component, OnInit } from '@angular/core';
import { MovieService } from '../shared/movies.service';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  imagePath: string;
  pageEvent: PageEvent;
  length = 0;
  pageSize = 20;
  showLoader: boolean;
  searchTxt: string;
  isSearchMovie: boolean;
  treandingMovies: boolean;
  movies: any = [];
  pageIndex: number;

  constructor(private mService: MovieService) { }

  ngOnInit() {
    this.imagePath = 'https://image.tmdb.org/t/p/w138_and_h175_face';
    this.mService.getSearchEmitter().subscribe((searchTxt) => {
      this.searchTxt = searchTxt;
      this.getMovies(1);
    });
    this.getMovies(1);
  }

  getMovies(p) {
    this.showLoader = true;
    if (this.searchTxt) {
      if (this.treandingMovies) { this.pageIndex = 0 };
      this.isSearchMovie = true;
      this.treandingMovies = false;
      this.mService.searchMovies(p, this.searchTxt).then((result) => {
        this.movies = result.results;
        this.length = result.total_results;
        this.showLoader = false;
      }).catch(err => this.showLoader = false);
    } else {
      if (this.isSearchMovie) { this.pageIndex = 0 };
      this.isSearchMovie = false;
      this.treandingMovies = true;
      this.mService.getTrendingMovies(p).then((result) => {
        this.movies = result.results;
        this.length = result.total_results;
        this.showLoader = false;
      }).catch(err => this.showLoader = false);
    }
  }

  mPageEvent(event) {
    this.getMovies(event.pageIndex + 1);
  }

  trimString(string, count) {
    if (string.length > count) {
      return string.substring(0, count) + '...';
    } else {
      return string;
    }
  }

  openMovie(movie) {
    this.mService.emitMovieEvent(movie);
  }

  imgError(image){
    image.srcElement.style.display = 'none';
  }
}
