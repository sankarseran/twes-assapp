import { Component, OnInit } from '@angular/core';
import { Subject, timer } from 'rxjs';
import { debounce } from 'rxjs/operators';
import { MovieService } from '../shared/movies.service';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {

  keyUp: Subject<string> = new Subject();

  constructor(private mService: MovieService) { }

  ngOnInit() {
    this.keyUp.pipe(debounce(() => timer(1000))).subscribe(val => this.mService.emitSearchEvent(val));
  }
}
