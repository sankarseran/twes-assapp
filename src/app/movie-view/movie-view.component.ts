import { Component, OnInit } from '@angular/core';
import { MovieService } from '../shared/movies.service';

@Component({
  selector: 'app-movie-view',
  templateUrl: './movie-view.component.html',
  styleUrls: ['./movie-view.component.css']
})
export class MovieViewComponent implements OnInit {
  selectedMovie: any;
  backgroundImg: string;
  imagePath: string;
  movie: any;
  showLoader: boolean;

  constructor(private mService: MovieService) { }

  ngOnInit() {
    this.mService.getMovieEmitter().subscribe((movie) => {
      this.selectedMovie = movie;
      this.getMovie();
    });
    this.imagePath = 'https://image.tmdb.org/t/p//w300_and_h450_bestv2';
    this.backgroundImg = 'https://image.tmdb.org/t/p/original';
  }

  getMovie() {
    this.showLoader = true;
    this.mService.getMovie(this.selectedMovie.id).then((result) => {
      this.movie = result;
      this.showLoader = false;
    }).catch(err => this.showLoader = false);
  }

  imgError(image){
    image.srcElement.style.display = 'none';
  }
}
